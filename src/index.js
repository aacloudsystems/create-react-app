import React from 'react';
import ReactDOM from 'react-dom';
import './styles.css';
import Header from './components/Header';
import Home from './components/Home';

function App() {
  return (
    <div className='App'>
      <Header />
      <Home />
    </div>
      
  );
}
const rootElement = document.getElementById("root");
ReactDOM.render(<App />, rootElement);
